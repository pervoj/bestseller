#!/usr/bin/env python

from src import required_python
from src.config import GETTEXT_PACKAGE
import os
import shutil
import venv

PROJECT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

VENV_DIR = os.path.join(PROJECT_DIR, "venv")

PO_DIR = os.path.join(PROJECT_DIR, "po")
MO_DIR = os.path.join(PROJECT_DIR, "build", "locales")


# PREPARE
# =======

# create venv if not exists
if not os.path.exists(VENV_DIR):
    eb = venv.EnvBuilder(with_pip = True)
    eb.create(VENV_DIR)

# run python command from venv
def venv_run(exe, args):
    os.system(f"{os.path.join(VENV_DIR, 'bin', exe)} {args}")

# install requirements
venv_run("pip", f"install -r {os.path.join(PROJECT_DIR, 'requirements.txt')}")

# clean
if os.path.exists(MO_DIR):
    shutil.rmtree(MO_DIR)
os.makedirs(MO_DIR)
if os.path.exists(os.path.join(PROJECT_DIR, "dist")):
    shutil.rmtree(os.path.join(PROJECT_DIR, "dist"))


# LOCALES
# =======

# install PyBabel
venv_run("pip", "install Babel")

template = os.path.join(PO_DIR, "template.pot")

# load all translatable files into one string
files = ""
with open(os.path.join(PO_DIR, "POTFILES"), "r", encoding = "utf-8") as f:
    for line in f.readlines():
        file = line.strip()
        if len(file) <= 0: continue
        files += f"{file} "

# build pot file
venv_run("pybabel", f"extract -o {template} -c \"Translators: \" {files}")

# set text domain variable in pot file
f = open(template, "r")
lines = "".join(f.readlines())
lines = lines.replace("PROJECT VERSION", GETTEXT_PACKAGE)
f = open(template, "w")
f.write(lines)

# update and compile po files
with open(os.path.join(PO_DIR, "LINGUAS"), "r", encoding = "utf-8") as f:
    for line in f.readlines():
        language = line.strip()
        if len(language) <= 0: continue
        file = f"{language}.po"
        file_path = os.path.join(PO_DIR, file)

        # generate file
        if os.path.exists(file_path): # update if exists
            venv_run("pybabel", f"update -D {GETTEXT_PACKAGE} -l {language} -i {template} -o {file_path}")
        else: # create if not
            venv_run("pybabel", f"init -D {GETTEXT_PACKAGE} -l {language} -i {template} -o {file_path}")

        # prepare paths
        path_po = os.path.join(PO_DIR, file)
        dir_mo = os.path.join(MO_DIR, language, "LC_MESSAGES")
        path_mo = os.path.join(dir_mo, f"{GETTEXT_PACKAGE}.mo")

        # create language directory in mo dir
        os.makedirs(dir_mo)

        # generate mo file
        venv_run("pybabel", f"compile -D {GETTEXT_PACKAGE} -l {language} -i {path_po} -o {path_mo}")

# remove pot file
os.remove(template)


# BUILD
# =====

# run PyInstaller
venv_run("pyinstaller", f"{os.path.join(PROJECT_DIR, 'bestseller.spec')}")
