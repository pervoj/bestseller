from pathlib import Path
import json
import jsonschema
from typing import List, TypedDict
from typing_extensions import NotRequired
from datetime import datetime

SCHEMA = {
    "type": "object",
    "properties": {
        "title": { "type": "string" },
        "basename": { "type": "string" },
        "authors": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": { "type": "string" },
                    "website": { "type": "string" },
                    "email": { "type": "string" },
                },
                "required": [ "name" ]
            },
            "minItems": 1,
            "uniqueItems": True
        },
        "language": { "type": "string" },
        "updated": {
            "type": "string",
            "format": "date-time"
        },
        "chapters": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "title": { "type": "string" },
                    "sources": {
                        "type": "array",
                        "items": { "type": "string" },
                        "minItems": 1
                    }
                },
                "required": [ "title", "sources" ]
            },
            "minItems": 1
        },
    },
    "required": [ "title", "authors", "language", "updated", "chapters" ]
}

class _BaseModel:
    def __init__(self, data):
        for key in data:
            setattr(self, key, data[key])

class _Author(_BaseModel):
    name: str
    website: NotRequired[str]
    email: NotRequired[str]

class _Chapter(_BaseModel):
    title: str
    sources: List[Path]

class ConfigParser:

    def __init__(self, config: Path):
        if not config.exists():
            raise FileNotFoundError()
        if not config.is_file():
            raise IsADirectoryError()
        self.__config = json.load(open(config))
        jsonschema.validate(self.__config, SCHEMA)
        self.__root_dir = config.parent.absolute()

    @property
    def title(self) -> str:
        return self.__config["title"]

    @property
    def basename(self) -> str:
        return self.__config["basename"]

    @property
    def authors(self) -> List[_Author]:
        authors: List[_Author] = []
        for author in self.__config["authors"]:
            authors.append(_Author(author))
        return authors

    @property
    def language(self) -> str:
        return self.__config["language"]

    @property
    def updated(self) -> datetime:
        return datetime.fromisoformat(self.__config["updated"])

    @property
    def chapters(self) -> List[_Chapter]:
        chapters: List[_Chapter] = []
        for chapter in self.__config["chapters"]:
            sources: List[str] = chapter["sources"]
            chapter["sources"] = []
            for source in sources: # convert all paths from str to Path
                chapter["sources"].append(self.__root_dir.joinpath("src", source))
            chapters.append(_Chapter(chapter))
        return chapters
