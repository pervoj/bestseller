from commands.command import Command
from buildutil import BuildUtil
from gettext import gettext as _
from pathlib import Path
import os
import colors
from json import JSONDecodeError
from jsonschema.exceptions import ValidationError

from config_parser import ConfigParser
from config import RESOURCE_DIR
import markdown

class Build(Command):
    name = "build"
    help = _("Builds the book")

    def init(self, subparser, default_group):
        default_group.add_argument("PATH",
            help = _("Bestseller project path"),
            nargs = "?",
            default = Path(os.getcwd()),
            type = Path
        )

    def parse(self, args):
        path: Path = args.PATH
        config = self.__parse_config(path.joinpath("config.json"))
        build = BuildUtil(config, path.absolute())
        build.build()

    def __parse_config(self, path: Path):
        try:
            return ConfigParser(path)
        except FileNotFoundError:
            colors.error(_("The config file does not exist!"))
        except IsADirectoryError:
            colors.error(_("The config file is not a file!"))
        except JSONDecodeError as e:
            colors.error(_("The config file is not a valid JSON!"))
            print(e)
        except ValidationError as e:
            colors.error(_("The config file is not valid!"))
            print(e.message)
        exit()
