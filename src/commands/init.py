from commands.command import Command
from gettext import gettext as _

class Init(Command):
    name = "init"
    help = _("Creates new Bestseller project")

    def init(self, subparser, default_group):
        pass

    def parse(self, args):
        pass
