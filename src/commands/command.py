from argparse import ArgumentParser, _ArgumentGroup, Namespace

class Command:
    name: str = ""
    help: str = ""

    def init(
        self,
        subparser: ArgumentParser,
        default_group: _ArgumentGroup
    ) -> None: pass

    def parse(self, args: Namespace) -> None: pass
