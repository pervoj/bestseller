import sys
import os
from pathlib import Path

VERSION = "0.1.0"

try:
    RESOURCE_DIR = sys._MEIPASS
    BUNDLE = True
except Exception:
    RESOURCE_DIR = str(Path(__file__).parents[1])
    BUNDLE = False

GETTEXT_PACKAGE = "bestseller"
LOCALE_DIR = os.path.join(RESOURCE_DIR, "locales") if BUNDLE else os.path.join(RESOURCE_DIR, "build", "locales")
