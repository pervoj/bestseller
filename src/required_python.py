import sys
if sys.version_info < (3, 10):
    print("Error: this program requires Python 3.10 or higher")
    exit(1)
