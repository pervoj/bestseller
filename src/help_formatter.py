from gettext import gettext as _
import argparse

class HelpFormatter(argparse.HelpFormatter):
    def __init__(self, *args, **kwargs):
        super(HelpFormatter, self).__init__(*args, **kwargs)

    def _format_usage(self, usage, actions, groups, prefix):
        return super(HelpFormatter, self)._format_usage(
            usage, actions, groups, ""
        )
