from config_parser import ConfigParser
from pathlib import Path
from typing import Dict, List
import shutil
import colors
from gettext import gettext as _
from hashlib import md5
from markdown import Markdown
from markdown.extensions.fenced_code import FencedCodeExtension
from markdown.extensions.toc import TocExtension
import weasyprint
from ebooklib import epub

class BuildUtil:

    def __init__(self, config: ConfigParser, dir: Path):
        self.__config = config

        dist = dir.joinpath("dist")
        build = dir.joinpath("build")

        self.__dirs: Dict[str, Path] = {
            "root": dir,
            "dist": dist,
            "build": build,
            "assets": dir.joinpath("assets"),
            "build:html": build.joinpath("html")
        }

    def __prepare(self) -> None:
        for key in self.__dirs:
            if key == "root": continue
            dir = self.__dirs[key]
            if dir.exists():
                shutil.rmtree(dir)
            dir.mkdir()

    def __sources(self) -> List[Path]:
        sources: List[Path] = []
        for chapter in self.__config.chapters:
            for source in chapter.sources:
                sources.append(source)
        return sources

    def __non_existent_sources(self) -> List[Path]:
        sources: List[Path] = []
        for source in self.__sources():
            if not source.exists():
                sources.append(source)
        return sources

    def __build_all_sources(self) -> Dict[Path, Path]:
        sources = self.__sources()
        built_sources: Dict[Path, Path] = {}

        # check all sources exists
        non_existent_sources = self.__non_existent_sources()
        if len(non_existent_sources) > 0:
            colors.error(_("Some source files does not exist!"))
            for source in non_existent_sources:
                print(source.absolute())
            exit()

        # build all sources
        for source in sources:
            # build the output file name from hash
            # to not conflict with other files
            hashed = md5(str(source).encode()).hexdigest()
            source_ext = "".join(source.suffixes)
            # prepare paths
            html = self.__dirs["build:html"].joinpath(hashed + ".html")
            built_sources[source] = html
            # convert the source
            md = Markdown(
                extensions = [
                    TocExtension(),
                    FencedCodeExtension()
                ],
                extension_configs = {
                    "toc": {
                        "marker": ""
                    }
                }
            )
            with open(source.absolute(), "r", encoding = "UTF-8") as f:
                source_contents = f.read()
            output_contents = md.convert(source_contents)
            # toc = md.toc_tokens
            with open(html.absolute(), "w", encoding = "UTF-8") as f:
                f.write(output_contents)

        return built_sources

    def __combine_built(self, built: Dict[Path, Path]) -> Path:
        output = self.__dirs["build:html"].joinpath("combined.html")
        # open the combined file
        with open(output, "w", encoding = "UTF-8") as o:
            # iterate through chapters
            for chapter in self.__config.chapters:
                content = ""
                # load each chapter source
                for source in chapter.sources:
                    with open(built[source], "r", encoding = "UTF-8") as i:
                        content += i.read() + "\n"
                # write the content to the output file
                o.write(f"<section>\n{content}</section>\n")
        return output

    def __build_pdf(self, built: Dict[Path, Path]) -> None:
        combined = self.__combine_built(built).absolute()
        output = self.__dirs["dist"].joinpath(f"{self.__config.basename}.pdf")
        # load HTML
        html = weasyprint.HTML(
            filename = combined,
            base_url = str(self.__dirs["assets"]),
            encoding = "UTF-8"
        )
        # generate PDF
        html.write_pdf(target = output)

    def __build_epub(self, built: Dict[Path, Path]) -> None:
        output = self.__dirs["dist"].joinpath(f"{self.__config.basename}.epub")
        assets = self.__dirs["assets"]
        # book metadata
        book = epub.EpubBook()
        book.set_title(self.__config.title)
        book.set_language(self.__config.language)
        # add authors
        for author in self.__config.authors:
            book.add_author(author.name)
        # load chapters
        chapters = []
        for i, chapter in enumerate(self.__config.chapters):
            # prepare chapter object
            ch = epub.EpubHtml(
                title = chapter.title,
                file_name = f"chapter-{i}.xhtml",
                lang = self.__config.language,
                content = ""
            )
            chapters.append(ch)
            # load the content from each source
            for source in chapter.sources:
                with open(built[source], "r") as f:
                    ch.content += f.read() + "\n"
            # add the chapter to the book
            book.add_item(ch)
        # list all assets
        for p in assets.rglob("*"):
            # read them and add them to the book
            with open(p, "rb") as f:
                book.add_item(epub.EpubItem(
                    file_name = str(p.relative_to(assets)),
                    content = f.read()
                ))
        # set chapters
        book.spine = chapters
        # write the file
        epub.write_epub(output, book, {})

    def build(self) -> None:
        self.__prepare()
        built = self.__build_all_sources()
        self.__build_pdf(built)
        self.__build_epub(built)
