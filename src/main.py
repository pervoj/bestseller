import required_python
from config import GETTEXT_PACKAGE, LOCALE_DIR, VERSION
import gettext
from gettext import gettext as _, pgettext
from argparse import ArgumentParser
from help_formatter import HelpFormatter
from typing import Type
from commands.command import Command
from commands.build import Build
from commands.init import Init

# load locales
gettext.bindtextdomain(GETTEXT_PACKAGE, LOCALE_DIR)
gettext.textdomain(GETTEXT_PACKAGE)


# create argument parser
# ======================

# instantiate default parser
parser = ArgumentParser(
    add_help = False,
    formatter_class = HelpFormatter,
    description = "\n" + _("Book Generator"),
    epilog = "https://gitlab.com/pervoj/bestseller"
)


# add default group
default_group = parser.add_argument_group(_("Options"))

# --help
default_group.add_argument("-h", "--help",
    action = "help",
    help = _("Show help information")
)
# --version
default_group.add_argument("-v", "--version",
    action = "store_true",
    help = _("Show version information")
)


# instantiate subcommands parser
subcommands = parser.add_subparsers(
    title = _("Subcommands"),
    help = _("Available subcommands")
)

# function to make the subcommand adding easier
def add_subcommand(command: Type[Command]):
    # instantiate subcommand
    subcommand = command()
    # instantiate parser
    subparser = subcommands.add_parser(
        subcommand.name,
        help = subcommand.help,
        add_help = False,
        formatter_class = HelpFormatter
    )
    # set parse function
    subparser.set_defaults(function = subcommand.parse)
    # add default group and --help
    default_group = subparser.add_argument_group(_("Options"))
    default_group.add_argument("-h", "--help",
        action = "help",
        help = _("Show help information")
    )
    # init the command
    subcommand.init(subparser, default_group)

# add subcommands
add_subcommand(Build)
add_subcommand(Init)


# parse args
# ==========

args = parser.parse_args()

# show version info
if args.version:
    print(pgettext("version information", "Bestseller {}").format(VERSION))
    exit(0)

if not hasattr(args, "function"):
    parser.print_help()
    exit(0)

# run subcommand parsing function
args.function(args)
